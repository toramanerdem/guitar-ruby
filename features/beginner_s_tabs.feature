Feature: Translation of guitar tabs
    As a music fan
    I would like to convert guitar tabs to music
    So that I can hear what they sound like

  Scenario: Playing Ode to joy, part 1
	Given I start my application
	And I load the following guitar tab
	  """
	  e|-------------------------------|
	  B|-5-5-6-8-8-6-5-3-1-1-3-5-5-3-3-|
	  G|-------------------------------|
	  D|-------------------------------|
	  A|-------------------------------|
	  E|-------------------------------|
	  """
	When the guitar plays
	Then the following notes should be played
	  """
	  B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B5 B3 B3
	  """

  Scenario: Playing Ode to joy, part 2
	Given I start my application
	And I load the following guitar tab
	  """
	  e|-------------------------------|
	  B|-5-5-6-8-8-6-5-3-1-1-3-5-3-1-1-|
	  G|-------------------------------|
	  D|-------------------------------|
	  A|-------------------------------|
	  E|-------------------------------|
	  """
    When the guitar plays
  	Then the following notes should be played
	  """
	  B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B3 B1 B1
	  """
