require 'midi_guitar'
require 'guitar'
require 'tab'
require 'cucumber/rspec/doubles'

Given(/^I start my application$/) do
  @midi = spy(MidiGuitar)
  @guitar = Guitar.new @midi
end

Given(/^I load the following guitar tab$/) do |string|
  @tab = Tab.new string
end

When(/^the guitar plays$/) do
  @guitar.play @tab
end

Then(/^the following notes should be played|generated$/) do |tabStr|
  expect(@midi).to have_received(:play).with(tabStr)
end
