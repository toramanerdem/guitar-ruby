require 'midi_guitar'
require 'guitar'
require 'tab'
require 'cucumber/rspec/doubles'


When(/^the guitar app parses the tab above$/) do
  @guitar.play @tab
end
