class Guitar
  attr_accessor :midiGuitar

  def play(tab)
    @midiGuitar.play(tab.to_notes)
  end



  def initialize(midiGuitar)
    @midiGuitar = midiGuitar
  end
end

