class Tab
  attr_accessor :tab_string


  def initialize(string)
    @tab_string = string
    @map = %w(e B G D A E)
  end

  def to_notes
    lines = @tab_string.strip.split("\n").select{|line| line !=''}
    lines.map!{|line| line.strip}
    howMany = lines.size / 6
    outString = []
    0.upto(howMany-1){|everySix|
      1.upto(lines[0].size) { |column|
        columnString =[]
        if(column % 2 == 1)
          0.upto(5){|index|
            whichLine = (everySix*6) + index
            line = lines[whichLine].strip
            element = line[column]
            if(element =~ /^\d$/)
              columnString.push @map[index]+element
            end
            if(element == '|')
              columnString.push element
            end
          }
          if !columnString.include?'|'
            columnString.push '_' if columnString.empty?
            outString.push(columnString.join '/')
          end
        end
      }
    }
    outString.join(' ')
  end
end
